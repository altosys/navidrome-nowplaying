FROM docker.io/alpine:3.16
COPY navidrome-nowplaying /opt/navidrome-nowplaying
RUN chmod a+x /opt/navidrome-nowplaying
RUN apk add -Uuq --no-cache tzdata ca-certificates

# ensure glibc compiled binary works with musl
RUN mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2

ENTRYPOINT ["/opt/navidrome-nowplaying"]
