# navidrome-nowplaying

navidrome-nowplaying is a simple sidecar container to expose Navidrome's "Now Playing" API without requiring client authentication. This is useful for allowing scripts like taskbar monitors to fetch the currently playing song information without credentials.

## Configuration

| Environment variable      | Description                              | Type   | Default              | Optional |
|---------------------------|------------------------------------------|--------|----------------------|----------|
| NAVIDROME_API_VERSION     | Navidrome (subsonic) API version         | String | 1.16.1               | Yes      |
| NAVIDROME_CLIENT_NAME     | Client name to pass to Navidrome         | String | navidrome-nowplaying | Yes      |
| NAVIDROME_PATH            | Pushover API getNowPlaying endpoint path | String | /rest/getNowPlaying  | Yes      |
| NAVIDROME_RESPONSE_FORMAT | Response format; json or xml             | String | json                 | Yes      |
| NAVIDROME_URL             | Navidrome URL (including "http://")      | String |                      | No       |
| NAVIDROME_USERNAME        | Navidrome username                       | String |                      | No       |
| NAVIDROME_PASSWORD        | Navidrome password                       | String |                      | No       |
