package main

import (
	"crypto/md5"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/spf13/viper"
)

func main() {
	// load environment variables
	if err := loadConfig(); err != nil {
		log.Fatal(err)
	}

	// start echo server
	e := echo.New()
	e.GET("/", nowPlaying)
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Logger.Fatal(e.Start(":8080"))
}

func loadConfig() error {
	prefix := "NAVIDROME"
	viper.SetEnvPrefix(prefix)
	viper.AutomaticEnv()
	for _, s := range []string{"USERNAME", "PASSWORD", "URL"} {
		if !viper.IsSet(s) {
			return fmt.Errorf(
				"required variable %s is not set",
				strings.Join([]string{prefix, s}, "_"),
			)
		}
	}
	viper.SetDefault("API_VERSION", "1.16.1")
	viper.SetDefault("CLIENT_NAME", "navidrome-nowplaying")
	viper.SetDefault("RESPONSE_FORMAT", "json")
	viper.SetDefault("PATH", "/rest/getNowPlaying")

	delimiter := strings.Repeat("-", 50)
	log.Println(delimiter)
	log.Println("Configuration:")
	log.Println("API Version", viper.GetString("API_VERSION"))
	log.Println("Client name:", viper.GetString("CLIENT_NAME"))
	log.Println("Navidrome URL:", viper.GetString("URL")+viper.GetString("PATH"))
	log.Println("Response format:", viper.GetString("RESPONSE_FORMAT"))
	log.Println("Username:", viper.GetString("USERNAME"))
	log.Println(delimiter)

	return nil
}

func nowPlaying(c echo.Context) error {
	URL, err := url.Parse(viper.GetString("URL"))
	if err != nil {
		return err
	}

	// generate salted authentication token
	salt := randStr(32)
	hash := md5.New()
	hash.Write([]byte(viper.GetString("PASSWORD") + salt))
	token := fmt.Sprintf("%x", string(hash.Sum(nil)))

	values := url.Values{}
	values.Add("c", viper.GetString("CLIENT_NAME"))
	values.Add("f", viper.GetString("RESPONSE_FORMAT"))
	values.Add("s", salt)
	values.Add("t", token)
	values.Add("u", viper.GetString("USERNAME"))
	values.Add("v", viper.GetString("API_VERSION"))
	URL.Path += viper.GetString("PATH")
	URL.RawQuery = values.Encode()

	resp, err := http.Get(URL.String())
	if err != nil {
		log.Println(err)
		return err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return err
	}
	return c.String(http.StatusOK, string(body))
}

func randStr(length int) string {
	rand.Seed(time.Now().UnixNano())
	b := make([]byte, length)
	// Read b number of numbers
	rand.Read(b)
	return fmt.Sprintf("%x", b)[:length]
}
